provider "aws" {
}

#VPC
module "vpc" {
source = "./modules/vpc/"
vpc_cidr = "192.168.0.0/16"
public_subnet1_cidr = "192.168.0.0/24"
public_subnet2_cidr = "192.168.1.0/24"
private_subnet1_cidr = "192.168.2.0/24"
private_subnet2_cidr = "192.168.3.0/24"
az1 = "us-east-1a"
az2 = "us-east-1b"
}

#RDS
module "rds" {
source = "./modules/rds/"
private1_subnet_id = module.vpc.private1_subnet_id
private2_subnet_id = module.vpc.private2_subnet_id
username = var.rds_login
password = var.rds_password
instance_class = "db.t3.micro"
sg_rds = module.vpc.sg_rds
}

#Endpoints
module "endpoints" {
source = "./modules/endpoints/"
sg_vpce = module.vpc.sg_vpce
vpc_id = module.vpc.vpc_id
private_route_table_id = module.vpc.private_route_table_id
public1_subnet_id = module.vpc.public1_subnet_id
public2_subnet_id = module.vpc.public2_subnet_id
}

#ECS
module "ecs" {
source = "./modules/ecs/"
execution_role_arn = module.endpoints.execution_role_arn
task_role_arn = module.endpoints.task_role_arn
private1_subnet_id = module.vpc.private1_subnet_id
private2_subnet_id = module.vpc.private2_subnet_id
sg_frontend = module.vpc.sg_frontend
sg_backend = module.vpc.sg_backend
sg_alb = module.vpc.sg_alb
public1_subnet_id = module.vpc.public1_subnet_id
public2_subnet_id = module.vpc.public2_subnet_id
vpc_id = module.vpc.vpc_id
}

module "auto_scaling" {
  source = "./modules/scaling"
  ecs_cluster = module.ecs.ecs_cluster
  backend_ecs_service = module.ecs.backend_ecs_service
  frontend_ecs_service = module.ecs.frontend_ecs_service
}

#Host zone record
module "dns" {
  source = "./modules/dns"
  zone_id = var.zone_id
  dns_name = var.dns_name
  alb_dns_name = module.ecs.alb_dns_name
  alb_zone_id = module.ecs.alb_zone_id
}

