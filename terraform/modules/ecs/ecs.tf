#ECS cluster
resource "aws_ecs_cluster" "project" {
  name = "ecs_project"
}
#ECS Cloudwatch
resource "aws_cloudwatch_log_group" "frontend" {
  name = "/ecs/frontend"
  tags = {
    Name        = "Frontend"
  }
}
resource "aws_cloudwatch_log_group" "backend" {
  name = "/ecs/backend"
  tags = {
    Name        = "Backend"
  }
}
#ECS Task Definition
resource "aws_ecs_task_definition" "frontend" {
  family                   = "frontend"
  execution_role_arn       = var.execution_role_arn
  task_role_arn            = var.task_role_arn
  container_definitions = <<DEFINITION
    [
      {
        "name": "frontend-container",
        "image": "154516445551.dkr.ecr.us-east-1.amazonaws.com/frontend:latest",
        "entryPoint": [],
        "essential": true,
        "cpu": 512,
        "memory": 1024,
        "portMappings": [
        {
          "containerPort": 80,
          "hostPort": 80
        }
      ],
      "logConfiguration": {
                "logDriver": "awslogs",
                "options": {
                    "awslogs-region" : "us-east-1",
                    "awslogs-group" : "/ecs/frontend",
                    "awslogs-stream-prefix" : "frontend"
                }
            },
        "networkMode": "awsvpc"
      }
    ]
    DEFINITION
    requires_compatibilities = ["FARGATE"]
    network_mode             = "awsvpc"
    memory                   = "1024"
    cpu                      = "512"
    tags = {
      Name        = "Frontend"
    }
  }

  resource "aws_ecs_task_definition" "backend" {
    family                   = "backend"
    execution_role_arn       = var.execution_role_arn
    task_role_arn            = var.task_role_arn
    container_definitions = <<DEFINITION
      [
        {
          "name": "backend-container",
          "image": "154516445551.dkr.ecr.us-east-1.amazonaws.com/backend:latest",
          "entryPoint": [],
          "essential": true,
          "cpu": 512,
          "memory": 1024,
          "portMappings": [
          {
            "containerPort": 8080,
            "hostPort": 8080
          }
        ],
        "logConfiguration": {
                  "logDriver": "awslogs",
                  "options": {
                      "awslogs-region" : "us-east-1",
                      "awslogs-group" : "/ecs/backend",
                      "awslogs-stream-prefix" : "backend"
                  }
              },
          "networkMode": "awsvpc"
        }
      ]
      DEFINITION
      requires_compatibilities = ["FARGATE"]
      network_mode             = "awsvpc"
      memory                   = "1024"
      cpu                      = "512"
      tags = {
        Name        = "Backend"
      }
    }
#Fargate Service
resource "aws_ecs_service" "frontend-ecs-service" {
  name                 = "frontend-ecs-service"
  cluster              = aws_ecs_cluster.project.id
  task_definition      = aws_ecs_task_definition.frontend.id
  launch_type          = "FARGATE"
  scheduling_strategy  = "REPLICA"
  desired_count        = 1
  force_new_deployment = true
  network_configuration {
    subnets          = [var.private1_subnet_id, var.private2_subnet_id]
    assign_public_ip = false
    security_groups = [var.sg_frontend]
    }
  load_balancer {
    target_group_arn = aws_lb_target_group.frontend_target_group.arn
    container_name   = "frontend-container"
    container_port   = 80
    }
  depends_on = [aws_lb_listener.frontend_listener]
  }
  resource "aws_ecs_service" "backend-ecs-service" {
    name                 = "backend-ecs-service"
    cluster              = aws_ecs_cluster.project.id
    task_definition      = aws_ecs_task_definition.backend.id
    launch_type          = "FARGATE"
    scheduling_strategy  = "REPLICA"
    desired_count        = 1
    force_new_deployment = true
    network_configuration {
      subnets          = [var.private1_subnet_id, var.private2_subnet_id]
      assign_public_ip = false
      security_groups = [var.sg_backend]
      }
    load_balancer {
      target_group_arn = aws_lb_target_group.backend_target_group.arn
      container_name   = "backend-container"
      container_port   = 8080
      }
    depends_on = [aws_lb_listener.backend_listener]
    }


#Application load Balancer
  resource "aws_alb" "project_alb" {
    name               = "project-alb"
    internal           = false
    load_balancer_type = "application"
    subnets            = [var.public1_subnet_id, var.public2_subnet_id]
    security_groups    = [var.sg_alb]

    tags = {
      Name        = "Project ALB"
    }
  }

#Target groups
resource "aws_lb_target_group" "frontend_target_group" {
  name        = "frontend-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = var.vpc_id

  health_check {
    healthy_threshold   = "3"
    interval            = "300"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = "/"
    unhealthy_threshold = "2"
  }

  tags = {
    Name        = "Frontend-TG"
  }
}
resource "aws_lb_target_group" "backend_target_group" {
  name        = "backend-tg"
  port        = 8080
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = var.vpc_id

  health_check {
    healthy_threshold   = "3"
    interval            = "300"
    protocol            = "HTTP"
    port                = 8080
    matcher             = "404"
    timeout             = "3"
    path                = "/api/"
    unhealthy_threshold = "2"
  }

  tags = {
    Name        = "Backend-TG"
  }
}

#ALB listeners
resource "aws_lb_listener" "app_lb_listener_redirect" {
  load_balancer_arn = aws_alb.project_alb.id
  port     = 80
  protocol = "HTTP"
  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
resource "aws_lb_listener" "frontend_listener" {
  load_balancer_arn = aws_alb.project_alb.id
  port     = 443
    protocol = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "arn:aws:acm:us-east-1:154516445551:certificate/189a12ad-f16b-49b6-828d-b426ccf1d735"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.frontend_target_group.id
  }
  depends_on = [aws_lb_listener.backend_listener]
}
resource "aws_lb_listener" "backend_listener" {
  load_balancer_arn = aws_alb.project_alb.id
  port              = "8080"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "arn:aws:acm:us-east-1:154516445551:certificate/189a12ad-f16b-49b6-828d-b426ccf1d735"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.backend_target_group.id
  }
}
