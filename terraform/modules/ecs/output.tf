output "alb_dns_name" {
  value = aws_alb.project_alb.dns_name
}
output "alb_zone_id" {
  value = aws_alb.project_alb.zone_id
}

output "ecs_cluster" {
  value = aws_ecs_cluster.project
}

output "frontend_ecs_service" {
  value = aws_ecs_service.frontend-ecs-service
}
output "backend_ecs_service" {
  value = aws_ecs_service.backend-ecs-service
}