#Endpoints
resource "aws_vpc_endpoint" "s3" {
  vpc_id            = var.vpc_id
  service_name      = "com.amazonaws.us-east-1.s3"
  vpc_endpoint_type = "Gateway"
  route_table_ids   = [var.private_route_table_id]
  tags = {
    Name        = "s3-endpoint"
  }
}
resource "aws_vpc_endpoint" "dkr" {
  vpc_id              = var.vpc_id
  private_dns_enabled = true
  service_name        = "com.amazonaws.us-east-1.ecr.dkr"
  vpc_endpoint_type   = "Interface"
  security_group_ids = [
    var.sg_vpce,
  ]
  subnet_ids = [var.public1_subnet_id, var.public2_subnet_id]
  tags = {
    Name        = "dkr-endpoint"
  }
}
resource "aws_vpc_endpoint" "dkr_api" {
  vpc_id              = var.vpc_id
  private_dns_enabled = true
  service_name        = "com.amazonaws.us-east-1.ecr.api"
  vpc_endpoint_type   = "Interface"
  security_group_ids = [
    var.sg_vpce,
  ]
  subnet_ids = [var.public1_subnet_id, var.public2_subnet_id]
  tags = {
    Name        = "dkr-api-endpoint"
  }
}
resource "aws_vpc_endpoint" "logs" {
  vpc_id              = var.vpc_id
  private_dns_enabled = true
  service_name        = "com.amazonaws.us-east-1.logs"
  vpc_endpoint_type   = "Interface"
  security_group_ids = [
    var.sg_vpce,
  ]
  subnet_ids = [var.public1_subnet_id, var.public2_subnet_id]
  tags = {
    Name        = "logs-endpoint"
  }
}

resource "aws_vpc_endpoint" "secretsmanager" {
  vpc_id              = var.vpc_id

  private_dns_enabled = true
  service_name        = "com.amazonaws.us-east-1.secretsmanager"
  vpc_endpoint_type   = "Interface"
  security_group_ids = [
    var.sg_vpce,
  ]
  subnet_ids = [var.public1_subnet_id, var.public2_subnet_id]

  tags = {
    Name        = "secretsmanager-endpoint"
  }
}

resource "aws_vpc_endpoint" "ssm" {
  vpc_id              = var.vpc_id

  private_dns_enabled = true
  service_name        = "com.amazonaws.us-east-1.ssm"
  vpc_endpoint_type   = "Interface"
  security_group_ids = [
    var.sg_vpce,
  ]
  subnet_ids = [var.public1_subnet_id, var.public2_subnet_id]

  tags = {
    Name        = "ssm-endpoint"
  }
}

resource "aws_vpc_endpoint" "kms" {
  vpc_id              = var.vpc_id

  private_dns_enabled = true
  service_name        = "com.amazonaws.us-east-1.kms"
  vpc_endpoint_type   = "Interface"
  security_group_ids = [
    var.sg_vpce,
  ]
  subnet_ids = [var.public1_subnet_id, var.public2_subnet_id]

  tags = {
    Name        = "kms-endpoint"
  }
}

#IAM polocies
data "aws_iam_policy_document" "fargate-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs.amazonaws.com", "ecs-tasks.amazonaws.com"]
    }
  }
}
resource "aws_iam_policy" "fargate_execution" {
  name   = "fargate_execution_policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
        "Effect": "Allow",
        "Action": [
            "ecr:GetDownloadUrlForLayer",
            "ecr:BatchGetImage",
            "ecr:BatchCheckLayerAvailability"
        ],
        "Resource": "*"
    },
    {
        "Effect": "Allow",
        "Action": [
            "ecr:GetAuthorizationToken"
        ],
        "Resource": "*"
    },
    {
        "Effect": "Allow",
        "Action": [
            "logs:CreateLogGroup",
            "logs:CreateLogStream"
        ],
        "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "fargate_task" {
  name   = "fargate_task_policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
        "Effect": "Allow",
        "Action": [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents"
        ],
        "Resource": "*"
    }
  ]
}
EOF
}
resource "aws_iam_role" "fargate_execution" {
  name               = "fargate_execution_role"
  assume_role_policy = data.aws_iam_policy_document.fargate-role-policy.json
}
resource "aws_iam_role" "fargate_task" {
  name               = "fargate_task_role"
  assume_role_policy = data.aws_iam_policy_document.fargate-role-policy.json
}
resource "aws_iam_role_policy_attachment" "fargate-execution" {
  role       = aws_iam_role.fargate_execution.name
  policy_arn = aws_iam_policy.fargate_execution.arn
}
resource "aws_iam_role_policy_attachment" "fargate-task" {
  role       = aws_iam_role.fargate_task.name
  policy_arn = aws_iam_policy.fargate_task.arn
}
