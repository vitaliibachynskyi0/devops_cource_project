output "execution_role_arn" {
  value = aws_iam_role.fargate_execution.arn
}
output "task_role_arn" {
  value = aws_iam_role.fargate_task.arn
}
