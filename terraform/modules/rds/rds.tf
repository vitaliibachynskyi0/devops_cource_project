resource "aws_db_subnet_group" "rds-subnet" {
  name       = "rds-subnet"
  subnet_ids = [var.private1_subnet_id,var.private2_subnet_id]
  tags = {
    Name = "My DB subnet group"
  }
}
resource "aws_db_instance" "mydb" {
  backup_retention_period  = 7   # in days
  db_subnet_group_name   = aws_db_subnet_group.rds-subnet.id
  engine                   = "postgres"
  engine_version           = "10.20"
  identifier               = "mydb"
  instance_class           = var.instance_class
  multi_az                 = false
  allocated_storage     = 20
  max_allocated_storage = 100
  port                     = 5432
  #final_snapshot_identifier = "latest"
  skip_final_snapshot = true
  storage_encrypted        = true
  storage_type             = "gp2"
  username                 = var.username
  password                 = var.password
  vpc_security_group_ids   = [var.sg_rds]
  }