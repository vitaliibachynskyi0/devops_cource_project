output "vpc_id"{
    value = aws_vpc.project-vpc.id
}
output "public1_subnet_id"{
    value = aws_subnet.project-public1-subnet.id
}
output "public2_subnet_id"{
    value = aws_subnet.project-public2-subnet.id
}
output "private1_subnet_id"{
    value = aws_subnet.project-private1-subnet.id
}
output "private2_subnet_id"{
    value = aws_subnet.project-private2-subnet.id
}
output "private_route_table_id"{
    value = aws_route_table.project-rt-private.id
}
output "sg_backend"{
    value = aws_security_group.project_backend.id
}
output "sg_frontend"{
    value = aws_security_group.project_frontend.id
}
output "sg_alb"{
    value = aws_security_group.project_alb.id
}
output "sg_rds"{
    value = aws_security_group.project_rds.id
}
output "sg_vpce"{
    value = aws_security_group.vpce.id
}