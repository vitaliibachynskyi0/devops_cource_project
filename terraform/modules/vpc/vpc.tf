resource "aws_vpc" "project-vpc" {
  cidr_block           = var.vpc_cidr
  instance_tenancy                    = "default"
  enable_dns_support                  = "true"
  enable_dns_hostnames                = "true"
  assign_generated_ipv6_cidr_block    = "false"
}
resource "aws_subnet" "project-public1-subnet" {
  vpc_id                  = aws_vpc.project-vpc.id
  cidr_block              = var.public_subnet1_cidr
  availability_zone       = var.az1
  map_public_ip_on_launch = true
  tags = {
    Name = "project public subnet 1"
  }
}
resource "aws_subnet" "project-public2-subnet" {
  vpc_id                  = aws_vpc.project-vpc.id
  cidr_block              = var.public_subnet2_cidr
  availability_zone       = var.az2
  map_public_ip_on_launch = true
  tags = {
    Name = "project public subnet 2"
  }
}
resource "aws_subnet" "project-private1-subnet" {
  vpc_id                  = aws_vpc.project-vpc.id
  cidr_block              = var.private_subnet1_cidr
  availability_zone       = var.az1
  map_public_ip_on_launch = false
  tags = {
    Name = "project private subnet 1"
  }
}
resource "aws_subnet" "project-private2-subnet" {
  vpc_id                  = aws_vpc.project-vpc.id
    cidr_block              = var.private_subnet2_cidr
  availability_zone       = var.az2
  map_public_ip_on_launch = false
  tags = {
    Name = "project private subnet 2"
  }
}
resource "aws_internet_gateway" "project-igw" {
  vpc_id = aws_vpc.project-vpc.id
  tags = {
    Name = "Project IGW"
  }
}
resource "aws_route_table" "project-rt-public" {
  vpc_id = aws_vpc.project-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.project-igw.id
  }
  tags = {
    Name = "Project Route Table Public"
  }
}
resource "aws_route_table_association" "project-rt-public-assoc1" {
  subnet_id      = aws_subnet.project-public1-subnet.id
  route_table_id = aws_route_table.project-rt-public.id
}
resource "aws_route_table_association" "project-rt-public-assoc2" {
  subnet_id      = aws_subnet.project-public2-subnet.id
  route_table_id = aws_route_table.project-rt-public.id
}

resource "aws_route_table" "project-rt-private" {
  vpc_id = aws_vpc.project-vpc.id
  tags = {
    Name = "Project Route Table Private"
  }
}
resource "aws_route_table_association" "project-rt-private-assoc1" {
  subnet_id      = aws_subnet.project-private1-subnet.id
  route_table_id = aws_route_table.project-rt-private.id
}
resource "aws_route_table_association" "project-rt-assoc2" {
  subnet_id      = aws_subnet.project-private2-subnet.id
  route_table_id = aws_route_table.project-rt-private.id
}

#Security Groups
resource "aws_security_group" "project_alb" {
  name        = "project_alb"
  description = "HTTPS"
  vpc_id      = aws_vpc.project-vpc.id
  dynamic "ingress" {
    for_each = ["80", "443", "8080"]
    content {
      from_port = ingress.value
      to_port = ingress.value
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Frontend SG"
  }
}
resource "aws_security_group" "project_frontend" {
  name        = "project_frontend"
  description = "HTTP"
  vpc_id      = aws_vpc.project-vpc.id
  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Frontend SG"
  }
}
resource "aws_security_group" "project_backend" {
  name        = "project_backend"
  description = "HTTP"
  vpc_id      = aws_vpc.project-vpc.id
  ingress {
    description = "HTTP"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Backend SG"
  }
}
resource "aws_security_group" "project_rds" {
  name        = "project_rds"
  description = "Postgres"
  vpc_id      = aws_vpc.project-vpc.id
  ingress {
    description = "Postgres"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "RDS SG"
  }
}

#Endpoint SG
resource "aws_security_group" "vpce" {
  name   = "vpce"
  vpc_id = aws_vpc.project-vpc.id
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }
  tags = {
    Name = "VPC endpoint SG"
  }
}




