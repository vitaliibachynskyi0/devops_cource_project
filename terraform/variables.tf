variable "rds_login" {
  type     = string
  nullable = false
}
variable "rds_password" {
  type     = string
  nullable = false
}
variable "zone_id" {
  type     = string
  nullable = false
}
variable "dns_name" {
  type     = string
  nullable = false
}
